package BankAccount;

public class BankAccount {
    private double balance;
    private double percent;

    BankAccount(double balance, double percent) {
        this.balance = balance;
        this.percent = percent;
    }

    @SuppressWarnings("unused")
    public double getBalance() {
        return balance;
    }

    @SuppressWarnings("unused")
    public double getPercent() {
        return percent;
    }

    /**
     * Метод поиска процентной ставки
     *
     * @return процентная ставка
     */
    double searchInterestRate() {
        return balance + balance * percent;
    }

    @Override
    public String toString() {
        return "<Банковский аккаунт>{" +
                "Баланс= " + balance + "руб. " +
                ", Процентная ставка= " + percent + "%" +
                '}';
    }
}
