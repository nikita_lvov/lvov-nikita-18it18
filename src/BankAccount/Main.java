package BankAccount;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс, показывающий возможности работы с объектами класса BankAccount
 *
 * @author Nikita Lvov 18it18
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<BankAccount> accounts = new ArrayList<>();// создаем ArrayList
        accounts.add(inputClient());// вызываем метод заполнения банковского акканута
        profit(accounts);// вызываем метод показывающий баланс с учтеом накоплений по процентной ставки
    }

    /**
     * Метод заполнения банковского акканута
     *
     * @return банковский аккаунт
     */
    private static BankAccount inputClient() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите баланс клиента  -  ");
        double balance = scanner.nextDouble();
        System.out.print("Введите процентную ставку - ");
        double percent = scanner.nextDouble();
        return new BankAccount(balance, percent);
    }

    /**
     * Метод показывающий баланс с учтеом накоплений по процентной ставки
     *
     * @param accounts банковский аккаунт
     */
    private static void profit(ArrayList<BankAccount> accounts) {
        for (BankAccount account : accounts) {
            System.out.println("Состояние счета с учетом процентной ставки = " + account.searchInterestRate());
        }
    }
}
