package Box;

public class Box {
    private String name;
    private int width;
    private int height;
    private int depth;

    public Box(String name, int width, int height, int depth) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public Box(String name,int side) {
        this(name, side, side, side);
    }

    public Box() {
        this(" коробочка ",1);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    @Override
    public String toString() {
        return name +
                " {width=" + width +
                ", height=" + height +
                ", depth=" + depth +
                '}';
    }

    public int volume() {
        return width * height * depth;
    }
}

