package Box;

/**
 * Список цветов коробки:
 * <ul>
 *    <li>White - белый</li>
 *    <li>Yellow - желтый</li>
 *    <li>Red - красный</li>
 *    <li>Blue - синий</li>
 *    <li>Black - черный</li>
 *    <li>Green - зеленый</li>
 * </ul>
 **/
public enum Color {
    WHITE,
    YELLOW,
    RED,
    BLUE,
    BLACK,
    GREEN
}
