package Box;

public class ColorBox extends Box {
    private Color color;

    public ColorBox(String name, int width, int height, int depth, Color color) {
        super(name, width, height, depth);
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "ColorBox{" +
                super.toString() +
                "color=" + color +
                '}';
    }
}
