package Box;

public class HeavyBox extends ColorBox {
    private int mass;

    public HeavyBox(String name, int width, int height, int depth, Color color, int mass) {
        super(name, width, height, depth, color);
        this.mass = mass;
    }

    public HeavyBox() {
        this("Тяжелая", 1, 1, 1, Color.WHITE, 1);
    }

    public int getMass() {
        return mass;
    }

    public void setMass(int mass) {
        this.mass = mass;
    }

    @Override
    public String toString() {
        return "Mass{" +
                super.toString() +
                "mass=" + mass +
                '}';
    }
}
