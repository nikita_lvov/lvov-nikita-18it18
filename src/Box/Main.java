package Box;

public class Main {
    public static void main(String[] args) {

        Box safe = new Box("сейф", 5, 7, 9);
        Box boxCube = new Box("кубик", 5);
        Box box = new Box();
        ColorBox colorBox = new ColorBox("Цветная коробка", 4, 6, 8, Color.YELLOW);
        HeavyBox heavyBox = new HeavyBox("Тяжелая коробка", 10, 5, 7, Color.BLACK, 100);
        Box[] boxes = {safe, boxCube, box, colorBox, heavyBox};
/*
        System.out.println(box);
        System.out.println("Ширина сейфа равна " + safe.getWidth() + " м.");
        safe.setWidth(3);
        System.out.println("Ширина сейфа равна " + safe.getWidth() + " м.");
*/
        System.out.println("Первая тяжелая " + heavyBox);
        System.out.println("Первая цветная " + colorBox);
        System.out.println(boxCube);
        System.out.println(box);

        int vSafe = safe.volume();
        System.out.println("Объем сейфа равен " + vSafe + " куб. м.");
        int vBoxCube = boxCube.volume();
        System.out.println("Объем куба равен " + vBoxCube + " куб. м.");
        int vBox = box.volume();
        System.out.println("Объем коробки равен " + vBox + " куб. м.");

        Box[] cubes = {safe, boxCube, box};

        MaxVolumeBox(vSafe, vBoxCube, vBox, cubes);
        outArrayOfBox(cubes);
    }

    private static void outArrayOfBox(Box[] cubes) {
        for (int i = 0; i < cubes.length; i++) {
            System.out.println((i + 1) + " коробочка " + cubes[i]);

        }
    }

    private static void MaxVolumeBox(int vSafe, int vBoxCube, int vBox, Box[] cubes) {
        int max = 0;
        for (int i = 0; i < cubes.length; i++) {
            max = Math.max(max, cubes[i].volume());
        }
        if (vSafe == max) {
            System.out.println("Самый большой объем у сейфа");
        }
        if (vBoxCube == max) {
            System.out.println("Самый большой объем у куба");
        }
        if (vBox == max) {
            System.out.println("Самый большой объем у коробки");
        }
    }
}
