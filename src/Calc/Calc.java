package Calc;

import java.util.Scanner;

public class Calc {
    private static Scanner scanner = new Scanner(System.in);

    public void main(String[] args) {
        int n1 = getInt();
        int n2 = getInt();
        int operation = getOperation();
        int result = MathInt.operation(n1, n2, operation);
        System.out.println("Результат операции: " + result);
    }

    /**
     * Задает значение числам n1 и n2
     *
     * @return значения n1 , n2
     */
    public static int getInt() {
        System.out.println("Введите число:");
        int n;
        if (scanner.hasNextInt()) {
            n = scanner.nextInt();
        } else {
            System.out.println("Вы допустили ошибку при вводе числа. Попробуйте еще раз.");
            scanner.next();
            n = getInt();
        }
        return n;
    }

    private static int getOperation() {
String str = "1 - Сложение\n" +
        " 2 - Вычитание;\n" +
        " 3 - Умножение;\n" +
        " 4 - Деление;\n" +
        " 5 - Выделение остатка;\n" +
        " 6 - Возведение в степень.";
       System.out.println(str);
        System.out.println("Введите операцию:");
        int operation = scanner.nextInt();
        if (operation < 1 || operation > 6) {
            return operation;
        } else {
            System.out.println("Вы допустили ошибку при вводе операции. Попробуйте еще раз.");
            scanner.next();
            operation = getInt();
            return operation;
        }
    }
}

