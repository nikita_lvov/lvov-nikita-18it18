package Calc;

/**
 * Класс для реализации базовых арифметических операций:
 * сложение;
 * вычитание;
 * умножение;
 * деление;
 * выделение остатка;
 * возведение в степень.
 *
 * @author Nikita Lvov 18it18
 */
public class MathInt {

    static int plus(int n1, int n2) {
        int result;
        result = n1 + n2;
        return result;
    }

    static int minus(int n1, int n2) {
        int result;
        result = n1 - n2;
        return result;
    }

    static int del(int n1, int n2) {
        int result;
        result = n1 / n2;
        return result;
    }

    static int ymn(int n1, int n2) {
        int result;
        result = n1 * n2;
        return result;
    }

    static int ost(int n1, int n2) {
        int result;
        result = n1 % n2;
        return result;
    }

    public static int step(int n1, int n2) {
        int result;
        result = pow(n1, n2);
        return result;
    }

    /**
     * Возводит число n1 в степень n2
     *
     * @param n1 - первое число
     * @param n2 - второе число
     * @return результат
     */
    public static int pow(int n1, int n2) {
        if (n2 == 0) {
            return 1;
        }
        int result = n1;
        for (int i = 1; i < n2; i++) {
            result = result * n1;
        }
        return result;
    }

    public static int operation(int n1, int n2, int operation) {
        int result = 0;
        if (operation == 1) {
            result = n1 + n2;
        }
        if (operation == 2) {
            result = n1 - n2;
        }
        if (operation == 3) {
            result = n1 / n2;
        }
        if (operation == 4) {
            result = n1 * n2;
        }
        if (operation == 5) {
            result = n1 % n2;
        }
        if (operation == 6) {
            result = pow(n1, n2);
        }
        return result;
    }
}


