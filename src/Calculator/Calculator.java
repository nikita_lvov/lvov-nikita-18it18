package Calculator;

import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {
        double result = 0.0; // объявляем и обнуляем переменную инт во избежание ошибок
        double n1 = 0.0;
        double n2 = 0.0;

        do { // выполнение цикла
            Scanner calcScan = new Scanner(System.in);  // Объявляем сканнер

            System.out.println("Введите пример. Например 2+2");

            if (calcScan.findInLine("(-?\\d+\\.?\\d*)?\\s*(\\S)\\s*(-?\\d+\\.?\\d*)")
                    != null) { // Проверить, нужного ли формата строка

                if (calcScan.match().group(1) != null) { //первое число из калькулятора
                    n1 = Double.parseDouble(calcScan.match().group(1)); //первое число из калькулятора
                    n2 = Double.parseDouble(calcScan.match().group(3)); //второе число из калькулятора
                    result = chooseOperation(n1, n2, calcScan.match().group(2)); //символ между числами
                    String operation = calcScan.match().group(2);

                    System.out.println(n1 + " " + operation + " " + n2 + " = " + result); //вывод результата
                }

            } else {
                System.out.println("Ошибка при вводе примера");//вывод ошибки
            }

        } while (true); //выполнение цикла

    }

    /**
     * Выбирает операцию и вызывает класс MathInt для подсчета
     *
     * @param n1        - первое число
     * @param n2        - второе число
     * @param operation - операция
     * @return результат операции над числами n1,n2
     */
    public static double chooseOperation(double n1, double n2, String operation) {
        double result = 0.0;
        switch (operation) {
            case "+":
                result = MathInt.add(n1, n2);
                break;
            case "-":
                result = MathInt.ded(n1, n2);
                break;
            case "*":
                result = MathInt.inc(n1, n2);
                break;
            case "/":
                result = MathInt.div(n1, n2);
                break;
            case "%":
                result = MathInt.dis(n1, n2);
                break;
            case "^":
                result = MathInt.ere(n1, n2);
                break;
            default:
                System.out.println("Операция не распознана.");
                break;
        }
        return result;
    }
}

