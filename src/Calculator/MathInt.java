package Calculator;

/**
 * Класс для реализации базовых арифметических операций:
 * сложение;
 * вычитание;
 * умножение;
 * деление;
 * выделение остатка;
 * возведение в степень.
 *
 * @author Nikita Lvov 18it18
 */
public class MathInt {
    /**
     * Складывет число n1 с числом n2
     *
     * @param n1 - первое число
     * @param n2 - второе число
     * @return сумма чисел n1,n2
     */
    public static double add(double n1, double n2) {
        double result;
        result = n1 + n2;
        return result;
    }

    /**
     * Вычитает из числа n1 число n2
     *
     * @param n1 - первое число
     * @param n2 - второе число
     * @return разница чисел n1,n2
     */
    public static double ded(double n1, double n2) {
        double result;
        result = n1 - n2;
        return result;
    }

    /**
     * Делит число n1 на число n2
     *
     * @param n1 - первое число
     * @param n2 - второе число
     * @return частное чисел n1,n2
     */
    public static double div(double n1, double n2) {
        double result;
        result = n1 / n2;
        return result;
    }

    /**
     * Умножает число n1 на число n2
     *
     * @param n1 - первое число
     * @param n2 - второе число
     * @return произведение чисел n1,n2
     */
    public static double inc(double n1, double n2) {
        double result;
        result = n1 * n2;
        return result;
    }

    /**
     * Выделяет остаток
     *
     * @param n1 - первое число
     * @param n2 - второе число
     * @return остаток
     */
    public static double dis(double n1, double n2) {
        double result;
        result = n1 % n2;
        return result;
    }

    /**
     * Возводит число n1 в степень n2
     *
     * @param n1 - первое число
     * @param n2 - второе число
     * @return число n1 в степени n2
     */
    public static double ere(double n1, double n2) {
        double result;
        result = pow(n1, n2);
        return result;
    }

    /**
     * Возводит число n1 в степень n2
     *
     * @param n1 - первое число
     * @param n2 - второе число
     * @return число n1 в степени n2
     */
    public static double pow(double n1, double n2) {
        if (n2 == 0) {
            return 1;
        }
        double result = n1;
        if (n2 > 0) {
            for (int i = 2; i <= n2; i++) {
                result = result * n1;
            }
            return result;
        }
    else if (n2 < 0) {
            for (int i = -2; i >= n2; i--) {
                result = result * n1;
            }
        }
        return 1 / result;
    }
}





