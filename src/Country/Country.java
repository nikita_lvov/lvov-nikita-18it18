package Country;

/**
 * Класс с параметрами стран
 *
 * @author Nikita Lvov 18it18
 */

import java.util.ArrayList;

public class Country {
    private String nameOfCountry;
    private String capitalOfCountry;
    private double s;
    private int population;

    /**
     * Конструктор стран
     *
     * @param nameOfCountry    - имя страны
     * @param capitalOfCountry - столица страны
     * @param s                - площадь страны
     * @param population       - население страны
     */
    public Country(String nameOfCountry, String capitalOfCountry, double s, int population) {
        this.nameOfCountry = nameOfCountry;
        this.capitalOfCountry = capitalOfCountry;
        this.s = s;
        this.population = population;
    }

    /**
     * Коструктор страны по стандатру
     */
    public Country() {
        this("неизвестно", "неизвестно", 0, 0);
    }

    /**
     * Геттер имени страны
     *
     * @return имя страны
     */

    public String getNameOfCountry() {
        return nameOfCountry;
    }

    /**
     * Геттер столицы страны
     *
     * @return столица страны
     */
    public String getCapitalOfCountry() {
        return capitalOfCountry;
    }

    /**
     * Геттер площади страны
     *
     * @return площадь страны
     */
    public double getS() {
        return s;
    }

    /**
     * Геттер населения страны
     *
     * @return население страны
     */
    public int getPopulation() {
        return population;
    }

    /**
     * Метод подсчёта плотности населения
     *
     * @return
     */
    public int populationDensity() {
        return (int) (population / s);
    }

    /**
     * Вывод информации про страну в строковом виже
     *
     * @return информация о стране
     */
    @Override
    public String toString() {
        return "{" +
                "Название страны - '" + nameOfCountry + '\'' +
                ", Столица страны - '" + capitalOfCountry + '\'' +
                ", Площадь страны = " + s +
                ", Население страны = " + population +
                ", Плотность населения страны = " + populationDensity() +
                '}';
    }
}
