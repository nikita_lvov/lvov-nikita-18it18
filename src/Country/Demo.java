package Country;
/**
 * Класс реализации поиска страны с самой большой плотностью населения
 *
 * @author Nikita Lvov 18it18
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Country russia = new Country("Россия", "Москва", 17125191, 146780720);// создаем объект с параметрами(страну Россия)
        Country usa = new Country("США", "Вашингтон", 9826675, 328915700);// создаем объект с параметрами(страну США)
        Country france = new Country("Франция", "Париж", 640679, 65060692);// создаем объект с параметрами(страну Франция)
        System.out.println("Введите количество стран: ");
        int num = sc.nextInt();
        ArrayList<Country> inputCountry = new ArrayList<>();// создаем ArrayList и присваиваем ему название
        for (int i = 0; i < num; i++) { // цикл повторения равному num
            inputCountry.add(input());// добавляем в ArrayList новую страну с параметрами из метода input()
        }
        inputCountry.add(russia);// добавляем в ArrayList страну Россия
        inputCountry.add(usa);// добавляем в ArrayList страну Сша
        inputCountry.add(france);// добавляем в ArrayList страну Франция

        int index = maxPopulationDensity(inputCountry);// сохраняем индекс страны с наибольшей плотностью населения

        System.out.println("Страна с наибольшей плотностью населения - " + inputCountry.get(index).toString());
    }

    /**
     * Метод ввода параметров страны
     * @return параметры о новой стране
     */
    private static Country input() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите название страны: ");
        String nameOfCountry = sc.nextLine();

        System.out.println("Введите столицу страны: ");
        String capitalOfCountry = sc.nextLine();

        System.out.println("Введите площадь страны: ");
        double s = sc.nextDouble();

        System.out.println("Введите население страны: ");
        int population = sc.nextInt();

        return new Country(nameOfCountry, capitalOfCountry, s, population);
    }

    /**
     * Метод реализующий поиск страны с наибольшей плотностью населения
     * @param inputCountry - ArrayList со странами
     * @return индекс страны с наибольшей плотностью населения
     */
    private static int maxPopulationDensity(ArrayList<Country> inputCountry) {
        double max = 0;
        int indexForMax = 0;
        for (int i = 0; i < inputCountry.size(); i++) {
            double density = inputCountry.get(i).populationDensity();
            if (max < density) {
                max = density;
                indexForMax = i;
            }
        }
        return indexForMax;
    }
}
