package Drob2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Rational result;
        System.out.print("Введите выражение a/b (знак операции) a1/b1 : ");
        String expression = sc.nextLine();
        String[] array = expression.split(" ");
        String[] variables1 = (array[0]).split("/");
        int num1 = Integer.parseInt(variables1[0]);
        int den1 = Integer.parseInt(variables1[1]);
        Rational rational1 = new Rational(num1, den1);
        String exp = array[1];
        String[] variables2 = (array[2]).split("/");
        int num2 = Integer.parseInt(variables2[0]);
        int den2 = Integer.parseInt(variables2[1]);
        Rational rational2 = new Rational(num2, den2);
        result = operation(rational1, rational2, exp);
        System.out.println(result.simplify());
    }

    private static Rational operation(Rational rational1, Rational rational2, String exp) {
        Rational rational3 = new Rational();
        switch (exp) {
            case "+":
                rational3 = rational1.add(rational2);
                break;

            case "-":
                rational3 = rational1.sub(rational2);
                break;

            case "/":
                rational3 = rational1.div(rational2);
                break;

            case "*":
                rational3 = rational1.mul(rational2);
                break;

            default:
                System.out.println("Введен не правильный знак операции!!!");

        }
        return rational3;
    }
}
