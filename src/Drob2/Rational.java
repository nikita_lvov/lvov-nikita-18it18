package Drob2;

public class Rational {
    private int num;
    private int den;

    Rational(int num, int den) {
        this.num = num;
        this.den = den;
        simplify();
    }

    Rational() {
        this(1, 1);
    }
    @SuppressWarnings("unused")
    public int getNum() {
        return num;
    }
    @SuppressWarnings("unused")
    public int getDen() {
        return den;
    }

    Rational add(final Rational r) {
        return new Rational(num * r.den + den * r.num, den * r.den).simplify();
    }

    Rational sub(final Rational r) {
        return new Rational(num * r.den - den * r.num, den * r.den).simplify();
    }

    Rational mul(final Rational r) {
        return new Rational(num * r.num, den * r.den).simplify();
    }

    Rational div(final Rational r) {
        return new Rational(num * r.den, den * r.num).simplify();
    }

    public String toString() {
        return num + "/" + den;
    }
    @SuppressWarnings("unused")
    public static int nod(int num, int den) {
        while (num != den) {
            if (num > den) {
                num = num - den;
            } else {
                den = den - num;
            }
        }
        return num;
    }

     Rational simplify() {
        long limit = Math.min(num, den);

        for (long i = 2; i <= limit; i++) {
            if (num % i == 0 && den % i == 0) {
                num /= i;
                den /= i;
            }
        }
        return this;
    }
}
