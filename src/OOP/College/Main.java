package OOP.College;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс, пказывающий возможности работы с объктами родительского класса Person
 *
 * @author Nikita Lvov 18it18
 */
public class Main {
    public static void main(String[] args) {

        Student student1 = new Student("Львов", Gender.МУЖСКОЙ, 2018, "09.02.07");
        Student student2 = new Student("Иванова", Gender.ЖЕНСКИЙ, 2017, "09.02.07");
        Student student3 = inputStudent();

        ArrayList<Student> students = new ArrayList<>();
        // создаем ArrayList и присваиваем ему название

        students.add(student1);
        students.add(student2);
        students.add(student3);


        Teacher teacher1 = new Teacher("Петрова", Gender.ЖЕНСКИЙ, "Математика", true);
        Teacher teacher2 = new Teacher("Ванин", Gender.МУЖСКОЙ, "Русский язык", false);
        Teacher teacher3 = inputTeacher();

        ArrayList<Teacher> teachers = new ArrayList<>();
        // создаем ArrayList и присваиваем ему название

        teachers.add(teacher1);
        teachers.add(teacher2);
        teachers.add(teacher3);


        ArrayList<Person> persons = new ArrayList<>();

        persons.add(student1);
        persons.add(student2);
        persons.add(student3);
        persons.add(teacher1);
        persons.add(teacher2);
        persons.add(teacher3);

        countStudentGirls(students);

        System.out.println("Сведения о преподователях-кураторах: ");
        infOfTeacherCurator(teachers);

        System.out.println("Сведения о преподователях и студентах мужского пола: ");
        infOfPersonMale(persons);
    }

    /**
     * Метод создающий и заполняющий объект Student(студент)
     *
     * @return новый объект Student(студент)
     */
    private static Student inputStudent() {
        Scanner sc = new Scanner(System.in);
        Gender gender = Gender.НЕИЗВЕСТЕН;

        System.out.println("Введите фамилию студента: ");
        String surname = sc.nextLine();
        System.out.println("Введите код специальности: ");
        String code = sc.nextLine();
        System.out.println("Введите год поступления ");
        int years = sc.nextInt();

        System.out.print("Введите пол студента(1-Мужской.2-Женский): ");
        int numberOfGender = sc.nextInt();
        if (numberOfGender == 1) {
            gender = Gender.МУЖСКОЙ;
        }
        if (numberOfGender == 2) {
            gender = Gender.ЖЕНСКИЙ;
        }

        return new Student(surname, gender, years, code);

    }

    /**
     * Метод создающий и заполняющий объект Teacher(преподаватель)
     *
     * @return новый объект Teacher(преподаватель)
     */
    private static Teacher inputTeacher() {
        Scanner sc = new Scanner(System.in);
        Gender gender = Gender.НЕИЗВЕСТЕН;

        System.out.println("Введите фамилию преподователя: ");
        String surname = sc.nextLine();
        System.out.println("Введите дисциплину: ");
        String discipline = sc.nextLine();

        System.out.print("Введите пол преподователя(1-Мужской.2-Женский): ");
        int numberOfGender = sc.nextInt();
        if (numberOfGender == 1) {
            gender = Gender.МУЖСКОЙ;
        }
        if (numberOfGender == 2) {
            gender = Gender.ЖЕНСКИЙ;
        }
        System.out.println("Кураторство: ");
        boolean curator = sc.nextBoolean();

        return new Teacher(surname, gender, discipline, curator);

    }

    /**
     * Выводит количество девушек поступивших в 2017
     *
     * @param students студенты
     */
    private static void countStudentGirls(ArrayList<Student> students) {
        int count = 0;
        for (Student student : students) {
            if (student.getYears() == 2017 && student.getGender() == Gender.ЖЕНСКИЙ)
                count++;
        }
        System.out.println("Количество девушек поступивших в 2017 году: " + count);
    }

    /**
     * Выводит сведения о преподавателях-кураторах
     *
     * @param teachers преподаватели
     */
    private static void infOfTeacherCurator(ArrayList<Teacher> teachers) {
        for (Teacher teacher : teachers) {
            if (teacher.isCurator()) {
                System.out.println(teacher.toString());
            }
        }
    }

    /**
     * Выводит сведения о студентах и преподавателях мужского пола
     *
     * @param people студенты и преподаватели
     */
    private static void infOfPersonMale(ArrayList<Person> people) {
        for (Person person : people) {
            if (person.getGender() == Gender.МУЖСКОЙ) {
                System.out.println(person.toString());
            }

        }
    }
}

