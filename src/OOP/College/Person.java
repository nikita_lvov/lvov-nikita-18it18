package OOP.College;

/**
 * Родительский класс параметров человека
 *
 * @author Nikita Lvov 18it18
 */
@SuppressWarnings("unused")
class Person {
    private String surname;
    private Gender gender;

    Person(String surname, Gender gender) {
        this.surname = surname;
        this.gender = gender;
    }

    String getSurname() {
        return surname;
    }

    Gender getGender() {
        return gender;
    }
}
