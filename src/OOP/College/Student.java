package OOP.College;

/**
 * Класс параметров студента, наследующий класс Person
 *
 * @author Nikita Lvov 18it18
 */
public class Student extends Person {
    private int years;
    private String code;

    Student(String surname, Gender gender, int years, String code) {
        super(surname, gender);
        this.years = years;
        this.code = code;
    }

    int getYears() {
        return years;
    }

    @SuppressWarnings("unused")
    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "Студент{" +
                "Фамилия - " + getSurname() +
                ", Пол - " + getGender() +
                ", Год поступления - " + years +
                ", Код специальности - " + code +
                '}' + "\n";
    }
}
