package OOP.College;

/**
 * Класс параметров преподавателя, наследующий класс Person
 *
 * @author Nikita Lvov 18it18
 */
public class Teacher extends Person {
    private String discipline;
    private boolean curator;


    Teacher(String surname, Gender gender, String discipline, boolean curator) {
        super(surname, gender);
        this.discipline = discipline;
        this.curator = curator;
    }

    @SuppressWarnings("unused")
    public String getDiscipline() {
        return discipline;
    }

    /**
     * Метод проверки преподавателя на кураторство
     *
     * @return возращает true если преподаватель куратор
     */
    boolean isCurator() {
        return curator;
    }

    @Override
    public String toString() {
        return "Преподователь{" +
                "Фамилия - " + getSurname() +
                ", Пол - " + getGender() +
                ", Дисциплина - " + discipline + '\'' +
                ", Кураторство - " + curator +
                '}' + "\n";
    }
}
