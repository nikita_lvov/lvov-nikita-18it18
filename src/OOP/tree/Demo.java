package OOP.tree;

import java.util.Scanner;

/**
 * Реализация класса ёлка {@code Tree}
 *
 * @autor Nikita Lvov 18it18
 */

public class Demo {
    public static void main(String[] args) {
        Tree tree = inputTree();// создаем объект и присваиваем ему метод выдающий ёлку с параметрами
        System.out.println("Характеристика вашей ёлки: ");
        System.out.println(tree);
        System.out.println("Итоговая цена ёлки - " + tree.costOfTree(tree) + " руб.");
        System.out.println("Подбор размера ёлки исходя из бюджета: ");
        Tree tree1 = sizeMoney();// создаем объект и присваиваем ему метод выдающий ёлку с параметрами
        System.out.println(tree1);
    }

    /**
     * Метод заполняющий данные о ёлки
     *
     * @return новый объект - ёлка
     */
    private static Tree inputTree() {
        Scanner sc = new Scanner(System.in);
        Type type = null;
        int numberType;
        int size;
        do {
            System.out.println("Введите размер желаемой ёлки(в метрах 1-5): ");
            size = sc.nextInt();
        } while (size < 1 || size > 5); // повтор цикла при неверно введенных данных
        do {
            System.out.println("Введите тип ёлки(1-Искусственная,2-Натуральная): ");
            numberType = sc.nextInt();
            if (numberType == 1) {
                type = Type.ИСКУССТВЕННАЯ;
            }
            if (numberType == 2) {
                type = Type.НАТУРАЛЬНАЯ;
            }
        } while (numberType < 1 || numberType > 2);// повтор цикла при неверно введенных данных

        return new Tree(size, type);
    }

    /**
     * Метод заполняющий параметры ёлки и расчитывающий размер ёлки на основании бюджета
     *
     * @return новый объект - ёлка
     */
    private static Tree sizeMoney() {
        Scanner sc = new Scanner(System.in);
        int numberType;
        int money;
        int size = 0;
        Type type = null;
        do {
            System.out.print("Сколько у вас денег: ");
            money = sc.nextInt();
        } while (money < 0); // повтор цикла при неверно введенных данных
        do {
            System.out.println("Введите тип ёлки(1-Искусственная,2-Натуральная): ");
            numberType = sc.nextInt();
            if (numberType == 1) {
                type = Type.ИСКУССТВЕННАЯ; // 200 - цена за искусственную ёлку
                money = money - 200;
            }
            if (numberType == 2) {
                type = Type.НАТУРАЛЬНАЯ;
                money = money - 350; // 350 - цена за натуральную ёлку
            }
        } while (numberType < 1 || numberType > 2);// повтор цикла при неверно введенных данных
        if (money < 150) {
            System.out.println("Недостаточно денег");
        } else {
            size = money / 150; // 150 - цена за метр ёлки
            if (size > 5) { // лимит размера ёлки
                size = 5;
            }
        }
        return new Tree(size, type);
    }
}
