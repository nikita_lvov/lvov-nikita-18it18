package OOP.tree;

/**
 * Класс параметров ёлки
 *
 * @author Nikita Lvov 18it18
 */
@SuppressWarnings("unused")
public class Tree {
    private int size;
    private Type typeOfTree;

    Tree(int size, Type typeOfTree) {
        this.size = size;
        this.typeOfTree = typeOfTree;
    }

    /*
    Конструктор по умолчанию
     */
    public Tree() {
        this(1, Type.НАТУРАЛЬНАЯ);
    }

    private int getSize() {
        return size;
    }

    Type getTypeOfTree() {
        return typeOfTree;
    }

    @Override
    public String toString() {
        return "Ёлка{" +
                "Размер = " + size +
                ", Тип = " + typeOfTree +
                '}';
    }

    /**
     * Метод расчитывающий стоимость ёлки
     *
     * @param tree ёлка
     * @return цена ёлки
     */
    int costOfTree(Tree tree) {
        int cost;
        cost = tree.getSize() * 150;
        if (typeOfTree == Type.НАТУРАЛЬНАЯ) {
            cost = cost + 350;
        }
        if (typeOfTree == Type.ИСКУССТВЕННАЯ) {
            cost = cost + 200;
        }
        return cost;
    }
}