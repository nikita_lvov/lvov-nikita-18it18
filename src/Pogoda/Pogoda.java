package Pogoda;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс для расчета средней температуры за месяц;
 * самой высокой температуры;
 * самой низкой температуры;
 * самых теплых дней;
 * самых холодных дней месяца.
 *
 * @author Nikita Lvov 18it18
 */

public class Pogoda {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int numberOfDays;
        System.out.print("Введите количество дней:");
        numberOfDays = scanner.nextInt();
        double temperature[] = new double[numberOfDays];
        fillTemperature(temperature);
        double minT = minTemperature(temperature);
        double maxT = maxTemperature(temperature);
        System.out.printf("%n Средняя температура за месяц :%5.1f ℃%n", averageTemperature(temperature));
        System.out.printf("%n Минимальная температура месяца :%5.1f ℃%n", minTemperature(temperature));
        System.out.printf("%n Максимальная температура месяца :%5.1f ℃%n", maxTemperature(temperature));
        System.out.printf("%nСамые теплые дни месяца: ");
        outArray(maxDaysTemperature(temperature, maxT, numberOfDays));
        System.out.printf("%nСамые холодные дни месяца: ");
        outArray(minDaysTemperature(temperature, minT, numberOfDays));
    }

    /**
     * Заполняет массив температуры рандомными числами
     *
     * @param temperature название массива
     * @return рандомная температура
     */
    private static void fillTemperature(double temperature[]) {
        for (int i = 0, n = 1; i < temperature.length; i++, n++) {
            temperature[i] = -2 + (Math.random() * 20);
            System.out.printf("Температура " + n + " дня = %.1f ℃%n ", temperature[i]);
        }
    }

    /**
     * Возвращает среднее значение температур
     *
     * @param temperature массив температур
     * @return среднее значение температур
     */
    private static double averageTemperature(double temperature[]) {
        double sum = 0.0;
        for (double temperatureOfDay : temperature) {
            sum = sum + temperatureOfDay;
        }
        return sum / temperature.length;
    }

    /**
     * Находит минимальную температуру
     *
     * @param temperature массив температур
     * @return минимальная температура
     */
    private static double minTemperature(double temperature[]) {
        double min = temperature[0];
        for (int i = 1; i < temperature.length; i++) {
            min = Math.min(min, temperature[i]);
        }
        return min;
    }

    /**
     * Находит максимальную температуру
     *
     * @param temperature массив температур
     * @return максимальная температура
     */
    private static double maxTemperature(double temperature[]) {
        double max = temperature[0];
        for (int i = 1; i < temperature.length; i++) {
            max = Math.max(max, temperature[i]);
        }
        return max;
    }

    /**
     * Находит самые холодные дни
     *
     * @param temperature массив температур
     * @param minT        минимальная температура
     * @return самые холодные дни
     */
    private static int[] minDaysTemperature(double[] temperature, double minT, int numberOfDays) {
        int[] day = new int[numberOfDays];
        int j = -1;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] == minT) {
                j++;
                day[j] = i + 1;
            }
        }
        return day;
    }

    /**
     * Находит самые теплые дни
     *
     * @param temperature массив температур
     * @param maxT        максимальная температура
     * @return самые теплые дни
     */
    private static int[] maxDaysTemperature(double[] temperature, double maxT, int numberOfDays) {
        int[] day = new int[numberOfDays];
        int j = -1;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] == maxT) {
                j++;
                day[j] = i + 1;
            }
        }
        return day;
    }


    private static void outArray(int[] days) {
        for (int i = 0; i < days.length; i++) {
            System.out.print(days[i] + " ");
        }
    }
}






