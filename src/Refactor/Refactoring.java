package Refactor;

/**
 * Класс представления хранения чисел в памяти компьютера
 *
 * @author Nikita Lvov 18it18
 */
public class Refactoring {
    public static void main(String[] args) {
        int i = 389;
        integerNumber(i);
        i = -386;
        integerNumber(i);

        double d = 75.38;
        realNumber(d);
        d = Math.PI;
        realNumber(d);

        d = 5.0 / 7;
        thirdMethod(d);
        d += 300000;
        thirdMethod(d);
    }

    /**
     * Выводит формат хранения целого числа в памяти компьютера
     *
     * @param i целое число
     */

    private static void integerNumber(int i) {
        String intBits = Integer.toBinaryString(i);
        System.out.println("Разряды числа: " + intBits);
    }

    /**
     * Выводит формат хранения вещественного числа в памяти компьютера
     *
     * @param d вещественное число
     */

    private static void realNumber(double d) {
        String sResult;
        long numberBits = Double.doubleToLongBits(d);
        sResult = Long.toBinaryString(numberBits);
        System.out.println("Представление вещественного числа в формате чисел с плавающей точкой");

        System.out.format("Число: %5.2f\n", d);
        System.out.println("Формат чисел с плавающей точкой:");
        //ведущий ноль сокращен системой, поэтому его нужно восстановить
        System.out.println(d > 0 ? "0" + sResult : sResult);
    }

    /**
     * Демонстрирует потерю точности числа
     *
     * @param d вещественное число
     */

    private static void thirdMethod(double d) {
        System.out.format("Число: %10.16f\n", d);

    }
}