package Robot;

/**
 * <ul>
 *    <li>Up - вверх</li>
 *    <li>Right - вправо</li>
 *    <li>Down - вниз</li>
 *    <li>Left - налево</li>
 * </ul>
 */
public enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT
}
