package Robot;
/**
 * Класс мейн для запуска алгоритма передвижения робота
 *
 * @author Nikita Lvov 18it18
 */

import java.util.Scanner;

public class MainRobot {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Robot robot = input();// создаем объект и присваиваем параметры через метод input()
        System.out.println(robot);// выводим информацию (метод toString())

        System.out.println("Введите конечное положение робота: ");
        System.out.print("X = ");
        int endX = sc.nextInt();
        System.out.print("Y = ");
        int endY = sc.nextInt();


        robot.move(endX, endY);// вызываем метод move для объекта robot

        System.out.println("Состояние робота: ");
        System.out.println(robot);
    }

    /**
     * Метод заполняет с помощью сканнера параметры для начального положения робота
     *
     * @return параметры для объекта
     */
    private static Robot input() {
        Scanner sc = new Scanner(System.in);
        Direction direction = Direction.UP;// задаем начальное положение
        System.out.println("Введите начальное положение робота: ");
        System.out.print("X = ");
        int x = sc.nextInt();
        System.out.print("Y = ");
        int y = sc.nextInt();
        System.out.print("Direction = ");
        sc.nextLine();
        String lookAt = sc.nextLine();
        switch (lookAt) {
            case "вверх":
                direction = Direction.UP;
                break;
            case "вниз":
                direction = Direction.DOWN;
                break;
            case "вправо":
                direction = Direction.RIGHT;
                break;
            case "влево":
                direction = Direction.LEFT;
            default:
                break;
        }
        return new Robot(x, y, direction);
    }
}
