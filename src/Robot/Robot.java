package Robot;

/**
 * Класс для написания алгоритма передвижения робота
 *
 * @author Nikita Lvov 18it18
 */
public class Robot {
    private int x;
    private int y;
    private Direction direction;

    /**
     * Коструктор робота
     *
     * @param x         - координата x
     * @param y         - координата y
     * @param direction - направление робота
     */
    public Robot(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    /**
     * Конструктор стандартного робота
     */
    public Robot() {
        this(0, 0, Direction.UP);
    }

    /**
     * Геттер координаты X
     *
     * @return координата x
     */
    public int getX() {
        return x;
    }

    /**
     * Геттер координаты Y
     *
     * @return координата y
     */
    public int getY() {
        return y;
    }

    /**
     * Геттер направления робота
     *
     * @return направление робота
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * Информация о роботе в строковом виде
     *
     * @return информация о роботе
     */
    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                ", y=" + y +
                ", direction=" + direction +
                '}';
    }

    /**
     * Метод поворота робота направо
     */
    private void turnRight() {

        switch (direction) {
            case UP:
                direction = Direction.RIGHT;
                break;
            case DOWN:
                direction = Direction.LEFT;
                break;
            case LEFT:
                direction = Direction.UP;
                break;
            case RIGHT:
                direction = Direction.DOWN;
                break;
        }
    }

    /**
     * Метод поворота робота налево
     */
    private void turnLeft() {

        switch (direction) {
            case UP:
                direction = Direction.LEFT;
                break;
            case DOWN:
                direction = Direction.RIGHT;
                break;
            case LEFT:
                direction = Direction.DOWN;
                break;
            case RIGHT:
                direction = Direction.UP;
                break;
        }
    }

    /**
     * Метод шага робота на 1 ед.
     */
    private void Step() {

        switch (direction) {
            case UP:
                y++;
                break;
            case RIGHT:
                x++;
                break;
            case DOWN:
                y--;
                break;
            case LEFT:
                x--;
                break;
            default:
                break;
        }
    }

    /**
     * Метод передвижение робота по координатам
     *
     * @param endX - конечная координата x
     * @param endY - конечная координата y
     */
    public void move(int endX, int endY) {
        if (endX > x) {
            while (direction != Direction.RIGHT) {
                turnRight();
            }
            while (x != endX) {
                Step();
            }
        }
        if (endX < x) {
            while (direction != Direction.LEFT) {
                turnLeft();
            }
            while (x != endX) {
                Step();
            }
        }
        if (endY > y) {
            while (direction != Direction.UP) {
                turnRight();
            }
            while (y != endY) {
                Step();
            }
        }
        if (endY < y) {
            while (direction != Direction.DOWN) {
                turnLeft();
            }
            while (y != endY) {
                Step();
            }
        }
    }
}
