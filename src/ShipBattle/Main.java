package ShipBattle;

import java.util.Scanner;

/**
 * Класс показывающий возможности работы с объектом класса Ship
 *
 * @author Nikita Lvov 18it18
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int lengthShip = 1 + (int) (Math.random() * 4);// рандомный размер корабля
        int gateWay;// длина шлюза
        do {
            System.out.println("Введите длину шлюза(0-20): ");
            gateWay = sc.nextInt();
        } while (gateWay < 0 || gateWay > 20);

        Ship ship = new Ship(randomStart(lengthShip, gateWay), lengthShip);// создаем объект корабль

        ShipBattle(ship, gateWay);// вызываем метод игры
    }

    /**
     * Возвращает рандомную начальную координату корабля
     *
     * @param lengthShip размер корабля
     * @param gateWay    размер шлюза
     * @return начальную координату корабля
     */
    private static int randomStart(int lengthShip, int gateWay) {
        int random;
        do {
            random = 1 + (int) (Math.random() * gateWay);
        }
        while (random < 0 || random >= gateWay - lengthShip);

        return random;
    }

    /**
     * Выводит результаты игры пользователя и попытки
     *
     * @param ship    объект корабль
     * @param gateWay размер шлюза
     */
    private static void ShipBattle(Ship ship, int gateWay) {
        int shot;
        int tries = 0;
        String message = "Начало игры";
        Scanner sc = new Scanner(System.in);
        do {
            System.out.print("Введите координату выстрела(от 0 до " + (gateWay - 1) + "):");
            shot = sc.nextInt();
            if (shot > gateWay || shot <= 0) {
                System.out.println("Неверная координата!");
                continue;
            }
            message = ship.shotShip(shot);
            System.out.println(message);
            if (message.equals("Попал") || message.equals("Мимо")) {
                tries++;
            }
        }
        while (!message.equals("Потоплен"));
        System.out.println("Число попыток: " + tries);
    }
}
