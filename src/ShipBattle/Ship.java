package ShipBattle;

import java.util.ArrayList;

public class Ship {
    private ArrayList<Integer> locationShip;

    Ship(int startShipCord, int lengthShip) {
        locationShip = new ArrayList<>();
        for (int i = startShipCord; i < (startShipCord + lengthShip); i++) {
            locationShip.add(i);
        }
    }

    @Override
    public String toString() {
        return "Ship{" +
                "locationShip=" + locationShip +
                '}';
    }

    /**
     * Выводит результат выстрелов игрока
     *
     * @param shot координата выстрела игрока
     */
    String shotShip(int shot) {
        String message = "Мимо";
        if (locationShip.contains(shot)) {
            message = "Попал";
            locationShip.remove((Integer) shot);
        } else {
            if (locationShip.isEmpty()) {
                message = "Потоплен";
            }
        }
        return message;
    }
}

