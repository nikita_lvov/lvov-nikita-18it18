package ru.nik.baggage;

/**
 * Класс сведений о багаже
 *
 * @author Nikita Lvov 18it18
 */
public class Baggage {
    private String surname;// фамилия владельца багажа
    private int slot;// кол-во багажных мест
    private double mas;// вес багажа

    Baggage(String surname, int slot, double mas) {
        this.surname = surname;
        this.slot = slot;
        this.mas = mas;
    }

    @SuppressWarnings("unused")
    public Baggage() {
        this("не указано", 0, 0);
    }

    String getSurname() {
        return surname;
    }

    @SuppressWarnings("unused")
    public int getSlot() {
        return slot;
    }

    @SuppressWarnings("unused")
    public double getMas() {
        return mas;
    }

    /**
     * Метод проверяет есть ли у пассажира ручная кладь
     *
     * @return ручная кладь
     */
    boolean Clad() {
        return slot == 1 && mas <= 10;
    }
}
