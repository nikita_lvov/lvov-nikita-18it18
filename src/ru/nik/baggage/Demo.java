package ru.nik.baggage;

/**
 * Класс мейн выводит имена пассажиров с ручной кладью
 *
 * @author Nikita Lvov 18it18
 */
public class Demo {
    public static void main(String[] args) {

        Baggage bag1 = new Baggage("Сергеев", 1, 10);
        Baggage bag2 = new Baggage("Иванов", 3, 2);
        Baggage bag3 = new Baggage("Двойнев", 1, 5);
        Baggage bag4 = new Baggage("Чернов", 5, 20);
        Baggage bag5 = new Baggage("Петров", 3, 50);

        Baggage[] bag = {bag1, bag2, bag3, bag4, bag5};// массив багажей

        pasClad(bag);// вызываем метод
    }

    /**
     * Класс реализующий вывод фамилий пассажиров с ручной кладью
     *
     * @param bag массив багажей
     */
    private static void pasClad(Baggage[] bag) {
        for (Baggage baggage : bag) {
            if (baggage.Clad()) {
                System.out.println(baggage.getSurname() + " - С ручной кладью ");
            }
        }
    }
}
