package ru.nik.book;

/**
 * Класс информации о книге
 *
 * @author Nikita Lvov 18it18
 */
public class Book {
    private String surnameOfAuthor;
    private String nameOfBook;
    private int yearsBook;

    Book(String surnameOfAuthor, String nameOfBook, int yearsBook) {
        this.surnameOfAuthor = surnameOfAuthor;
        this.nameOfBook = nameOfBook;
        this.yearsBook = yearsBook;
    }

    @SuppressWarnings("unused")
    public Book() {
        this("неизвестно", "неизвестно", 0);
    }

    @SuppressWarnings("unused")
    public String getSurnameOfAuthor() {
        return surnameOfAuthor;
    }

    @SuppressWarnings("unused")
    public String getNameOfBook() {
        return nameOfBook;
    }

    int getYearsBook() {
        return yearsBook;
    }

    @Override
    public String toString() {
        return "Книга{" +
                "Автор книги: '" + surnameOfAuthor + '\'' +
                ", Название книги: '" + nameOfBook + '\'' +
                ", Год издания: " + yearsBook +
                '}';
    }

    /**
     * Метод сравнения текущей книги и полученной
     *
     * @param book книга
     * @return истина, если год издания книг равен
     */
    boolean isSameYears(Book book) {
        return this.yearsBook == book.getYearsBook();
    }
}
