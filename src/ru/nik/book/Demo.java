package ru.nik.book;

/**
 * Класс мейн реализующий
 * вывод книг 2018 года издания
 * вывод песен с категорией короткая
 * сравнение категории первой и последней песни
 *
 * @author Nikita Lvov 18it18
 */
public class Demo {
    public static void main(String[] args) {
        Book book1 = new Book("Сергеев", "b1", 2019);
        Book book2 = new Book("Иванов", "b2", 2018);
        Book book3 = new Book("Двойнев", "b3", 1987);
        Book book4 = new Book("Чернов", "b4", 2005);
        Book book5 = new Book("Петров", "b5", 2019);

        Book[] books = {book1, book2, book3, book4, book5};

        infOfYearsBook2018(books);
        sameYears(books);

    }

    /**
     * Метод проверяет есть ли книги с 2018 годом издания
     *
     * @param books массив кинг
     */
    private static void infOfYearsBook2018(Book[] books) {
        int count = 0;
        for (Book book : books) {
            if (book.getYearsBook() == 2018) {
                System.out.println(book);
                count++;
            }
        }
        if (count == 0)
            System.out.println("Нет книг с 2018 годом издания");
    }

    /**
     * Метод сравнивает попарно годы издания книг
     *
     * @param books массив книг
     */
    private static void sameYears(Book[] books) {
        int count = 0;
        for (int i = 0; i < 4; i++) {
            int n = i + 1;
            if (books[i].isSameYears(books[n]) || books[0].isSameYears(books[4])) {
                count++;
            }
        }
        if (count == 0) {
            System.out.println("Год издания различный");
        } else {
            System.out.println("Есть пары книг с одинаковым годом издания");
        }
    }
}

