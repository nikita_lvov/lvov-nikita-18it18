package ru.nik.figure;

/**
 * Класс который описывает фигуру круг
 *
 * @author Nikita Lvov 18it18
 */
public class Circle extends Shape {
    private Point center;//центр круга
    private double radius;//радиус круга

    /**
     * Конструктор квадрата
     *
     * @param color  цвет круга
     * @param center - центр круга
     * @param radius - радиус круга
     */
    public Circle(Color color, Point center, double radius) {
        super(color);
        this.center = center;
        this.radius = radius;
    }

    /**
     * Коструктор стандартного круга
     */
    public Circle() {
        this(Color.BLACK, new Point(0, 0), 0);
    }

    /**
     * Метод вычисления площади круга
     *
     * @return площадь круга
     */
    @Override
    public double area() {
        return Math.PI * radius * radius;
    }

    /**
     * Метод вывода строковой информации про круг
     *
     * @return информация про круг
     */
    @Override
    public String toString() {
        return "Круг{" +
                "цвет=" + getColor() +
                ", центр=" + center +
                ", радиус=" + radius +
                '}';
    }
}
