package ru.nik.figure;

/**
 * Класс мейн (в разработке)
 *
 * @author Nikita Lvov 18it18
 */
public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(Color.BLACK, new Point(), 5);
        Triangle triangle = new Triangle(Color.RED, new Point(0, 0),
                new Point(1, 0), new Point(0, 1));
        Square square = new Square(Color.WHITE, 5, new Point(1, 1));

        Shape[] shapes = {circle, triangle, square};
        printArrayElements(shapes);

        /*
        Shape maxShape = maxShapeArea(shapes);
        System.out.println("Фигура с наибольшей площадью:" + maxShape);

         */

    }

    private static void printArrayElements(Shape[] shapes) {
        for (int i = 0; i < shapes.length; i++) {
            System.out.println((i + 1) + " фигура " + shapes[i]);

        }
    }
    /*
    private static int maxShapeArea(Shape[] shapes) {
        double max = 0;
        int indexForMax = 0;
        for (int i = 0; i < shapes.length; i++) {
            double maxArea = shapes.area();
            if (max < maxArea) {
                max = maxArea;
                indexForMax = i;
            }
        }
        return indexForMax;
    }

     */
}
