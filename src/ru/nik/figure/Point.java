package ru.nik.figure;

/**
 * Класс для представления точки в двумерном пространстве
 *
 * @author Nikita Lvov 18it18
 */
public class Point {
    private double x;
    private double y;

    /**
     * Конструктор точки
     *
     * @param x - координата по x
     * @param y - координата по y
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Стандартный конструктор
     */
    public Point() {
        this(0, 0);
    }

    /**
     * Геттер x
     *
     * @return значение координаты x
     */
    public double getX() {
        return x;
    }

    /**
     * Геттер y
     *
     * @return значение координаты y
     */
    public double getY() {
        return y;
    }

    /**
     * Вывод информации про координаты точки
     *
     * @return информация о координатах точки
     */
    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
