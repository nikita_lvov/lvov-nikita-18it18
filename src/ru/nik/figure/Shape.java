package ru.nik.figure;

/**
 * Абстрактый класс для представления цветных фигур
 *
 * @author Nikita Lvov 18it18
 */
public abstract class Shape {
    private Color color;

    /**
     * Коструктор с параметром цвета
     *
     * @param color - цвет
     */

    public Shape(Color color) {
        this.color = color;
    }

    /**
     * Геттер цвета
     *
     * @return - цвет
     */
    public Color getColor() {
        return color;
    }

    /**
     * Абстрактный метод вычисления площади
     */
    public abstract double area();

}
