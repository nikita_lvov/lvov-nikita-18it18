package ru.nik.figure;

/**
 * Класс который описывает фигуру квадрат
 *
 * @author Nikita Lvov 18it18
 */
public class Square extends Shape {
    private double side;// сторона
    private Point corner;// угол

    /**
     * Конструктор квадрата
     *
     * @param color  - цвет
     * @param side   - сторона
     * @param corner - угол
     */
    public Square(Color color, double side, Point corner) {
        super(color);
        this.side = side;
        this.corner = corner;
    }

    /**
     * Коструктор стандартного квадрата
     */
    public Square() {
        this(Color.BLACK, 1, new Point(0, 0));
    }

    /**
     * Метод вычисления площади квадрата
     *
     * @return площадь квадрата
     */
    @Override
    public double area() {
        return side * side;
    }

    /**
     * Метод вывода строковой информации про квадрат
     *
     * @return информация про квадрат
     */
    @Override
    public String toString() {
        return "Квадрат{" +
                "цвет=" + getColor() +
                ", сторона=" + side +
                ", угол=" + corner +
                '}';
    }
}
