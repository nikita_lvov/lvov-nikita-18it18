package ru.nik.figure;

/**
 * Класс который описывает фигуру треугольник
 *
 * @author Nikita Lvov 18it18
 */
public class Triangle extends Shape {
    private Point a;
    private Point b;
    private Point c;

    /**
     * Конструктор квадрата
     *
     * @param color цвет квадрата
     * @param a     - точка a
     * @param b     - точка b
     * @param c     - точка c
     */
    public Triangle(Color color, Point a, Point b, Point c) {
        super(color);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * Коструктор стандартного треугольника
     */
    public Triangle() {
        this(Color.BLACK, new Point(0, 0), new Point(0, 0), new Point(0, 0));
    }

    /**
     * Метод вычисления площади треугольника
     *
     * @return площадь треугольника
     */
    @Override
    public double area() {
        return ((b.getX() - a.getX()) * (c.getY() - a.getY()) - (c.getX() - a.getX()) * (b.getY() - a.getY())) / 2;
    }

    /**
     * Метод вывода строковой информации про треугольник
     *
     * @return информация про треугольник
     */
    @Override
    public String toString() {
        return "Треугольник{" +
                "цвет=" + getColor() +
                ", a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}
