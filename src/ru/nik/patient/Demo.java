package ru.nik.patient;

/**
 * Класс для проверки диспансеризации пациентов
 *
 * @author Nikita Lvov 18it18
 */
public class Demo {
    public static void main(String[] args) {
        Patient patient1 = new Patient("Сергеев", 1989, 50, true);
        Patient patient2 = new Patient("Иванов", 2005, 51, true);
        Patient patient3 = new Patient("Двойнев", 1989, 52, false);
        Patient patient4 = new Patient("Чернов", 2001, 53, false);
        Patient patient5 = new Patient("Петров", 2000, 54, true);

        Patient[] patients = {patient1, patient2, patient3, patient4, patient5};

        infOfPatient(patients);
    }

    /**
     * Метод проверки диспансеризации пациентов рожденных до 2000 года
     *
     * @param patients - пациенты
     */
    private static void infOfPatient(Patient[] patients) {
        for (Patient patient : patients) {
            if (patient.isDis() && patient.getNumberOfBorn() < 2000) {
                System.out.println(patient.getSurname() + " - Прошел диспансеризацию \n " + "Информация о пациенте: " + patient.toString());
            }
        }
    }
}


