package ru.nik.patient;

/**
 * Класс сведений о пациенте
 *
 * @author Nikita Lvov 18it18
 */
public class Patient {
    private String surname;// фамилия пациента
    private int numberOfBorn;// год рождения пациента
    private int numberOfCard;// номер карточки пациента
    private boolean dis;// диспансеризация

    Patient(String surname, int numberOfBorn, int numberOfCard, boolean dis) {
        this.surname = surname;
        this.numberOfBorn = numberOfBorn;
        this.numberOfCard = numberOfCard;
        this.dis = dis;
    }

    @SuppressWarnings("unused")
    public Patient() {
        this("не указана", 0, 0, false);
    }

    String getSurname() {
        return surname;
    }

    int getNumberOfBorn() {
        return numberOfBorn;
    }

    @SuppressWarnings("unused")
    public int getNumberOfCard() {
        return numberOfCard;
    }

    boolean isDis() {
        return dis;
    }

    /**
     * Вывод сведений о пациенте
     *
     * @return сведения о пациенте
     */
    @Override
    public String toString() {
        return "Пациент{" +
                "Фамилия='" + surname + '\'' +
                ", год рождения=" + numberOfBorn +
                ", номер карты=" + numberOfCard +
                ", диспансеризация=" + dis +
                '}';
    }
}
