package ru.nik.song;

import java.util.Scanner;

/**
 * Класс мейн реализующий
 * вывод песен с заданной продолжительностью
 * вывод песен с категорией короткая
 * сравнение категории первой и последней песни
 *
 * @author Nikita Lvov 18it18
 */
public class Demo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Song song1 = new Song("Tic tac", "Lil krystal", 60);
        Song song2 = new Song("Bad bear", "T-fest", 20);
        Song song3 = new Song("Noticed", "Lil mosey", 280);
        Song song4 = new Song("Bad Guy", "Billie", 120);
        Song song5 = new Song("Old Town Road", "Lil Nas X", 50);

        Song[] songs = {song1, song2, song3, song4, song5};// массив песен

        System.out.println("Введите длинну песни : ");
        int songInput = sc.nextInt();
        infOfSong(songs, songInput);

        System.out.println("Информация о коротких песнях: ");
        infOfShortSong(songs);

        sameCategory(songs);


    }

    /**
     * Метод поиска песен с заданной продолжительностью
     *
     * @param songs     массив песен
     * @param songInput введенная пользователем продолжительность песни
     */
    private static void infOfSong(Song[] songs, int songInput) {
        int count = 0;
        for (Song song : songs) {
            if (song.getTimeOfSong() == songInput) {
                System.out.println(song);
                count++;
            }
        }
        if (count == 0) {
            System.out.println("Нет песен с заданной продолжительностью");
        }
    }

    /**
     * Метод поиска коротких песен и вывод информации о них
     *
     * @param songs массив песен
     */
    private static void infOfShortSong(Song[] songs) {
        for (Song song : songs) {
            if (song.Category().equals("Short")) {
                System.out.println(song);
            }
        }
    }

    /**
     * Метод, проверяющий относятся ли первая и последняя песня к одно категории
     *
     * @param songs массив песен
     */
    private static void sameCategory(Song[] songs) {
        if (songs[0].isSameCategory(songs[4])) {
            System.out.println("Песни относятся к одной категории");
        } else {
            System.out.println("Песни не относятся к одной категории");
        }
    }
}


