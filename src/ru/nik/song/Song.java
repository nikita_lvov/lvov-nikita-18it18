package ru.nik.song;

/**
 * Класс сведений о песне
 *
 * @author Nikita Lvov 18it18
 */
public class Song {
    private String nameOfSong;// название песни
    private String authorOfSong;// название автора
    private int timeOfSong;// продолжительность песни

    Song(String nameOfSong, String authorOfSong, int timeOfSong) {
        this.nameOfSong = nameOfSong;
        this.authorOfSong = authorOfSong;
        this.timeOfSong = timeOfSong;
    }

    @SuppressWarnings("unused")
    public Song() {
        this("не указана", "не указан", 0);
    }

    @SuppressWarnings("unused")
    public String getNameOfSong() {
        return nameOfSong;
    }

    @SuppressWarnings("unused")
    public String getAuthorOfSong() {
        return authorOfSong;
    }

    int getTimeOfSong() {
        return timeOfSong;
    }

    @Override
    public String toString() {
        return "Песня{" +
                "Название песни: '" + nameOfSong + '\'' +
                ", Автор песни: '" + authorOfSong + '\'' +
                ", Время песни: " + timeOfSong +
                '}';
    }

    /**
     * Метод проверки категории песни
     *
     * @return категория песни
     */
    String Category() {

        String category = "";

        if (timeOfSong < 120) {
            category = "Short";
        }
        if (timeOfSong > 120 && timeOfSong < 240) {
            category = "Medium";
        }
        if (timeOfSong > 240) {
            category = "Long";
        }
        return category;
    }

    /**
     * Метод сравнения текущей песни и полученной
     *
     * @param song песня
     * @return истина, если категория песен равна
     */
    boolean isSameCategory(Song song) {
        return this.Category().equals(song.Category());
    }
}
