package ru.nik.zadachi;

import java.util.Scanner;

public class Mass {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Укажите длину массива: ");
        int n = sc.nextInt();
        int[] mark = new int[n];
        for (int i = 0; i < mark.length; i++) {
            System.out.print("Введите числа: ");
            mark[i] = sc.nextInt();
        }
        int max = mark[0];
        int min = mark[0];
        int imax = 0;
        int imin = 0;
        for (int i = 1; i < mark.length; i++) {
            if (mark[i] > max) {
                max = mark[i];
                imax = i;
            }
            if (mark[i] < min) {
                min = mark[i];
                imin = i;
            }
        }
        mark[imin] = max;
        mark[imax] = min;
        for (int i = 0; i < mark.length; i++) {
            System.out.println("i=" + i + " mark[" + i + "]=" + mark[i]);
        }
    }
}