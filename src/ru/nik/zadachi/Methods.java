package ru.nik.zadachi;

public class Methods {
    public static void main(String[] args) {
        double x = 5;
        int n = 6;
        double z = 2;
        double y = (pow(x, 8) - pow(7, n)) / pow(z, 10);
        System.out.println("y= " + y);
    }

    private static double pow(double a, int b) {
        if (b == 0) {
            return 1;
        }
        double result = a;
        for (int i = 1; i < b; i++) {
            result = result * a;
        }
        return result;
    }
}
