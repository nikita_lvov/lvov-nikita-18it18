package ru.nik.zadachi;

public class Printer {
    public static void main(String[] args) {
        print("Hello");
        print("Пока!");
        int z = add(2, 4);
        int c = add(3, 9);
        System.out.println(z);
        System.out.println(c);
    }

    private static int add(int x, int y) {
        return x + y;
    }

    private static void print(String word) {
        System.out.println(word);
    }
}
