package ru.nik.zadachi;

import java.util.Scanner;

public class Rating {
    public static void main(String[] args) {
       /*
      1 - Длина массива и его элементы задаются изначально
        int[] mark = {5, 4, 3, 2, 2, 5, 5, 2, 5, 3, 3, 4, 2, 5, 4, 3, 3, 2, 5, 4, 5, 5, 4, 2, 4};
        */
       /*
      2 - Длина массива: 7 , элементы вводятся с клавиатуры
        int[] mark = new int[7];
       */
        // 3 - Длина массива и его элемены вводятся с клавиатуры
        Scanner sc = new Scanner(System.in);
        double s = 0;
        System.out.println("Укажите количество студентов: ");
        // ввод длины массива
        int n = sc.nextInt();
        int[] mark = new int[n];
        for (int i = 0; i < mark.length; i++) {
            System.out.print("Введите оценку: ");
            mark[i] = sc.nextInt();// заполняем массив с клавиатуры(вводим оценки)
        }

        for (int value : mark) {
            s = s + value;
        }
        s = s / mark.length;
        System.out.println("Средний балл равен " + s);
    }
}
