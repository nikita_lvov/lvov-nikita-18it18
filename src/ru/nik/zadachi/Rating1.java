package ru.nik.zadachi;

import java.util.Scanner;

public class Rating1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int two = 0;
        int five = 0;
        double s = 0;
        System.out.println("Укажите количество студентов: ");
        // ввод длины массива
        int n = sc.nextInt();
        int[] mark = new int[n];
        for (int i = 0; i < mark.length; i++) {
            System.out.print("Введите оценку: ");
            mark[i] = sc.nextInt();// заполняем массив с клавиатуры(вводим оценки)
            if (mark[i] == 2) {
                two++;
            }
            if (mark[i] == 5) {
                five++;
            }
        }
        int max = mark[0];
        int min = mark[0];
        for (int i = 1; i < mark.length; i++) min = Math.min(min, mark[i]);
        for (int i = 1; i < mark.length; i++) max = Math.max(max, mark[i]);
        System.out.println("Максимальная оценка: " + max);
        System.out.println("Минимальная оценка: " + min);
        for (int value : mark) {
            s = s + value;
        }
        s = s / mark.length;
        System.out.println("Средний балл равен " + s);
        System.out.println("Количество двоек: " + two);
        System.out.println("Количество пятёрок: " + five);
    }
}



