package ru.nik.zadachi;

public class Rating2 {
    public static void main(String[] args) {
        int[] mark = {2, 3, 14, 3, -4, 2};
        int max = mark[0];
        int min = mark[0];
        int imax = 0;
        int imin = 0;
        for (int i = 1; i < mark.length; i++) {
            if (mark[i] > max) {
                max = mark[i];
                imax = i;
            }
            if (mark[i] < min) {
                min = mark[i];
                imin = i;
            }
        }
        mark[imin] = max;
        mark[imax] = min;
        for (int i = 0; i < mark.length; i++) {
            System.out.println("i=" + i + " mark[" + i + "]=" + mark[i]);
        }
    }
}



