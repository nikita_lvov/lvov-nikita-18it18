package ru.nik.zadachi;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс для расчета среднего балла по группам
 *
 * @author T.A.Iniushkina
 */
public class Rating3 {
    private static final String GR15OIT18 = "15ОИТ18";
    private static final String GR15OIT20 = "15ОИТ20";
    private static final String FORMAT = "Средний балл в группе %s равен %5.2f%n";
    private static final String STRING = "Введите количество студентов в группе ";
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int numberOf15oit18 = getNumberOfGroup(GR15OIT18);
        int[] gr15oit18 = new int[numberOf15oit18];
        int[] gr15oit20 = new int[getNumberOfGroup(GR15OIT20)];
        init(gr15oit18);
        System.out.println(Arrays.toString(gr15oit18));
        init(gr15oit20);
        System.out.println(Arrays.toString(gr15oit20));
        double average15oit18 = average(gr15oit18);
        System.out.format(FORMAT, GR15OIT18, average15oit18);
        System.out.printf(FORMAT, GR15OIT20, average(gr15oit20));
        Math.addExact(2, 4);
    }

    /**
     * Возвращает количество студентов в группе
     *
     * @param nameOfGroup название группы
     * @return количество студентов в группе
     */
    private static int getNumberOfGroup(String nameOfGroup) {
        System.out.print(STRING + nameOfGroup + ":");
        return scanner.nextInt();
    }

    /**
     * Возвращает средний балл по группе
     *
     * @param group массив оценок группы
     * @return средний балл
     */
    private static double average(int[] group) {
        int sum = 0;
        for (int rating : group) {
            sum = sum + rating;
        }
        return (double) sum / group.length;
    }

    /**
     * Инициализирует массив оценок группы group значениями от 2 до 5
     *
     * @param group массив оценок группы
     */
    private static void init(int[] group) {
        for (int i = 0; i < group.length; i++) {
            group[i] = 2 + (int) (Math.random() * 4);
        }
    }
}
